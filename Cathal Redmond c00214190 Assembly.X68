    org $2000

*-------------------------------------------------------
*Assembly Project by Cathal Redmond C00214190
*-------------------------------------------------------

*-------------------------------------------------------
*Differet Variables used in the game
*-------------------------------------------------------

player_health           EQU 0 health of player
player_speed_level      EQU 20 what level upgrade speed of ship is at
player_weapons_level    EQU 40 what level upgrade weapons are at
crew_amount             EQU 60 how much crew the ship has
enemy_type              EQU 80 what type an enemy is denoted by a number
enemy_health            EQU 100 health of enemy
enemy_speed_level       EQU 120 speed of enemy
enemy_weapons_level     EQU 140 enemy weapons level - used for damge calulation
credit_amount           EQU 160 amount of credits(money) player has to spend
*Start of Game
start:
    move.b  #100,$4000 put score/health in memory location $4000
    lea     $4000,A3   assign address A3 to that memory location
*-------------------------------------------------------
*intializing different variables
*-------------------------------------------------------
    move.l  #5000, credit_amount        
    move.l  #1, player_speed_level
    move.l  #1, player_weapons_level
    move.l  #5, crew_amount
    move.l  #100, player_health
    
   
    bsr     intro       branch to intro subroutine - game continues from there
  
intro:          * subroutine that brings up intro msg
    lea     welcome_msg,A1
    move.b  #14,D0
    trap    #15
    bsr     endl
    bsr     decorate
    lea     intro_msg,A1
    move.b  #14,D0
    trap    #15
    bsr     decorate
    bsr     endl 
    bsr     citadel
    
citadel:        * subroutine that takes input from user and moves to either the upgrade or mission select
                * subroutine
    bsr     endl
    bsr     decorate
    lea     citadel_msg,A1
    move.b  #14,D0
    trap    #15
    move.b  #4, D0
    trap    #15
    cmp     #1, D1
    beq     upgrade
    cmp     #2, D1
    beq     mission_select
    bne     citadel  
*--------------------------------------------------------
*-------------------Upgrades-----------------------------
*--------------------------------------------------------
upgrade:        * subroutine that brings up the menu screen
                * and takes input to move to a certain upgrade
    bsr     endl
    bsr     decorate
    lea     upgrade_msg,A1
    move.b  #14,D0
    trap    #15
    move.b  #4,D0
    trap    #15
    cmp     #1, D1
    beq     weapon_upgrade
    cmp     #2,D1
    beq     shield_upgrade
    cmp     #3,D1
    beq     speed_upgrade
    cmp     #4,D1
    beq     crew_upgrade
    cmp     #5,D1
    beq     citadel
    bne     upgrade

weapon_upgrade:         * subroutine that brings up weapon confirmation upgrade screen
    bsr     endl
    bsr     decorate
    lea     credit_msg,A1
    move.b  #14,D0
    trap    #15
    move.l  credit_amount,D1
    move.b  #3,D0
    trap    #15
    bsr     endl
    lea     weapon_level_msg,A1
    move.b  #14, D0
    trap    #15
    move.l  #0, D1
    move.l  player_weapons_level,D1
    move.b  #3,D0
    trap    #15
    bsr     endl
    lea     weapon_upgrade_msg,A1
    move.b  #14, D0
    trap    #15
    bsr     endl
    move.b  #4,D0
    trap    #15
    cmp     #2,D1
    beq     upgrade
    cmp     #1,D1
    beq     weapon_level
    bne     weapon_upgrade
    
weapon_level:           * subroutine that updates values if weapons upgraded
    bsr     endl        
    bsr     decorate
    move.l  credit_amount,D4
    cmp     #500, D4
    blt     weapon_upgrade
    move.l player_weapons_level, D3
    add     #1,D3
    sub     #500, D4
    move.l  D4, credit_amount
    move.l  D3,player_weapons_level
    bge     weapon_upgrade
    
shield_upgrade:         * subroutine that brings up shields confirmation upgrade screen
                        
    bsr     endl
    bsr     decorate
    lea     credit_msg,A1
    move.b  #14,D0
    trap    #15
    move.l  credit_amount,D1
    move.b  #3,D0
    trap    #15
    bsr     endl
    lea     shield_level_msg,A1
    move.b  #14, D0
    trap    #15
    move.l  #0, D1
    move.l  player_health,D1        * shields relate to player health
    move.b  #3,D0
    trap    #15
    bsr     endl
    lea     shield_upgrade_msg,A1
    move.b  #14, D0
    trap    #15
    bsr     endl
    move.b  #4,D0
    trap    #15
    cmp     #2,D1
    beq     upgrade
    cmp     #1,D1
    beq     shield_level
    bne     shield_upgrade
    
shield_level:                   * subroutine that updates values if shields upgraded

    bsr     endl        
    bsr     decorate
    move.l  credit_amount,D4
    cmp     #500, D4
    blt     shield_upgrade
    move.l  player_health, D3
    add     #100,D3
    sub     #500, D4
    move.l  D4, credit_amount
    move.l  D3,player_health
    bge     shield_upgrade
    
speed_upgrade:              * subroutine that brings up speed confirmation upgrade screen

    bsr     endl
    bsr     decorate
    lea     credit_msg,A1
    move.b  #14,D0
    trap    #15
    move.l  credit_amount,D1
    move.b  #3,D0
    trap    #15
    bsr     endl
    lea     speed_level_msg,A1
    move.b  #14, D0
    trap    #15
    move.l  #0, D1
    move.l  player_speed_level,D1
    move.b  #3,D0
    trap    #15
    bsr     endl
    lea     speed_upgrade_msg,A1
    move.b  #14, D0
    trap    #15
    bsr     endl
    move.b  #4,D0
    trap    #15
    cmp     #2,D1
    beq     upgrade
    cmp     #1,D1
    beq     speed_level
    bne     speed_upgrade
    
speed_level:                * subroutine that updates values if speed upgraded


    bsr     endl        
    bsr     decorate
    move.l  credit_amount,D4
    cmp     #500, D4
    blt     speed_upgrade
    move.l  player_speed_level, D3
    add     #1,D3
    sub     #500, D4
    move.l  D4, credit_amount
    move.l  D3, player_speed_level
    bge     speed_upgrade
    
crew_upgrade:           * subroutine that brings up increase crew amount confirmation

    bsr     endl
    bsr     decorate
    lea     credit_msg,A1
    move.b  #14,D0
    trap    #15
    move.l  credit_amount,D1
    move.b  #3,D0
    trap    #15
    bsr     endl
    lea     crew_amount_msg,A1
    move.b  #14, D0
    trap    #15
    move.l  #0, D1
    move.l  crew_amount,D1
    move.b  #3,D0
    trap    #15
    bsr     endl
    lea     crew_upgrade_msg,A1
    move.b  #14, D0
    trap    #15
    bsr     endl
    move.b  #4,D0
    trap    #15
    cmp     #2,D1
    beq     upgrade
    cmp     #1,D1
    beq     crew_level
    bne     crew_upgrade
crew_level:                 * subroutine that updates values if crew amount increased

    bsr     endl        
    bsr     decorate
    move.l  credit_amount,D4
    cmp     #1000, D4
    blt     crew_upgrade
    move.l  crew_amount, D3
    add     #5,D3
    sub     #1000, D4
    move.l  D4, credit_amount
    move.l  D3,crew_amount
    bge     crew_upgrade

*------------------------------------------------------------
*--------Mission Selection/Enemy Detection-------------------
*------------------------------------------------------------
mission_select:             * subroutine takes input from player to choose a mission destination
    bsr     endl
    bsr     decorate
    lea     location_choice_msg,A1
    move.b  #14,D0
    trap    #15
    bsr     endl
    move.b  #4,D0
    trap    #15
    cmp     #1,D1
    beq     citadel
    move.l  D1, enemy_type
    cmp     #8,D1                 makes sure number is within range
    bgt     mission_select
    cmp     #1,D1                 makes sure number is within range
    blt     mission_select
    bsr     enemy_detected
    
enemy_detected:             * subroutine that outputs to screen that enemy is detected
    bsr     endl
    bsr     decorate
    lea     mission_select_msg,A1
    move.b  #14,D0
    trap    #15
    bsr     enemy_type_subroutine
    
enemy_type_subroutine:      * subroutine that determines which enemy type it is based on location
    move.l  enemy_type,D4
    cmp     #2,D4
    beq     bloodpack_enemy
    cmp     #3,D4
    beq     bluesuns_enemy
    cmp     #4,D4
    beq     batarian_enemy
    cmp     #5,D4
    beq     geth_enemy
    cmp     #6,D4
    beq     eclipse_enemy
    cmp     #7,D4
    beq     reapers_enemy
    cmp     #8,D4
    beq     cerberus_enemy
    
*-----------------------------------------------------------------------------------------------------
*Following subroutines sets up values for each enemy type and outputs to screen which enemy type it is
*-----------------------------------------------------------------------------------------------------    
bloodpack_enemy:
    lea     bloodpack_enemy_msg,A1
    move.b  #14,D0
    trap    #15
    move.l  #1,enemy_speed_level
    move.l  #1,enemy_weapons_level
    bsr     battle
bluesuns_enemy:
    lea     bluesuns_enemy_msg,A1
    move.b  #14,D0
    trap    #15
    move.l  #2,enemy_speed_level
    move.l  #1,enemy_weapons_level
    bsr     battle
batarian_enemy:
    lea     batarian_enemy_msg,A1
    move.b  #14,D0
    trap    #15
    move.l  #1,enemy_speed_level
    move.l  #1,enemy_weapons_level
    bsr     battle
geth_enemy:
    lea     geth_enemy_msg,A1
    move.b  #14,D0
    trap    #15
    move.l  #3,enemy_speed_level
    move.l  #2,enemy_weapons_level
    bsr     battle
eclipse_enemy:
    lea     eclipse_enemy_msg,A1
    move.b  #14,D0
    trap    #15
    move.l  #2,enemy_speed_level
    move.l  #1,enemy_weapons_level
    bsr     battle
reapers_enemy:
    lea     reaper_enemy_msg,A1
    move.b  #14,D0
    trap    #15
    move.l  #5,enemy_speed_level
    move.l  #3,enemy_weapons_level
    bsr     battle
cerberus_enemy:
    lea     cerberus_enemy_msg,A1
    move.b  #14,D0
    trap    #15
    move.l  #4,enemy_speed_level
    move.l  #2,enemy_weapons_level
    bsr     battle
*-----------------------------------------------------------
*--------------------Battle---------------------------------
*-----------------------------------------------------------
*subroutine that takes input from player what to do in battle   
battle:
    bsr     endl
    bsr     decorate
    mulu    #100,D4
    move.l  D4, enemy_health
    move.l  enemy_health,D4
    move.l  player_health,D5
    lea     battle_choice_msg,A1
    move.b  #14,D0
    trap    #15
    move.b  #4,D0
    trap    #15
    cmp     #1,D1
    beq     attack
    cmp     #2,D1
    beq     flee
    bne     battle
    
attack:     *subrotuine that allows player to attack enemy
    lea     enemy_health_msg,A1
    move.b  #14,D0
    trap    #15
    move.l  enemy_health,D1
    move.b  #3,D0
    trap    #15
    bsr     endl
    lea     player_health_msg,A1
    move.b  #14,D0
    trap    #15
    move.l  player_health,D1
    move.b  #3,D0
    trap    #15
    bsr     endl
    lea     battle_choice_msg,A1
    move.b  #14,D0
    trap    #15
    move.b  #4,D0
    trap    #15
    cmp     #2,D1
    beq     flee
    cmp     #1,D1
    beq     damage
    
damage:     *subroutine that deals damage to player and enemy after attacking
    move.l  player_weapons_level,D6
    move.l  enemy_weapons_level,D7
    mulu    #10,D6              damage equal to weapon level * 50
    sub     D6,D4
    cmp     #0, D4              checks if health is 0
    bls     win
    mulu    #10,D7              damage equal to weapon level * 50 - only take damage if enemy not dead
    sub     D7,D5
    cmp     #0, D5              checks if health is 0
    bls     lose
    move.l  D4,enemy_health
    move.l  D5,player_health
    bsr     attack


    
win:        * subroutine for when you win a battle
 
    move.l  #0,enemy_health

    lea     win_msg,A1
    move.b  #14,D0
    trap    #15
    move.l  credit_amount,D3
    move.l  enemy_type, D2
    mulu    250, D2         * when you win you receive more money
    add     D2,D3
    move.l  D3,credit_amount
    bsr     mission_select
lose:       * subroutine for when you lose a battle
    
    move.l  crew_amount,D3
    sub     #5,D3
    cmp     #0,D3
    beq     gameover
    lea     lose_msg,A1
    move.b  #14,D0
    trap    #15
    move.l  D3,crew_amount
    move.l  player_speed_level,D3
    sub     1,D3
    move.l  D3,player_speed_level
    move.l  #100,player_health
    
    move.l  player_weapons_level,D3
    sub     1,D3
    move.l  D3,player_weapons_level
    move.l  player_speed_level,D3
    sub     1,D3
    move.l  D3,player_speed_level
    bsr     mission_select
    
gameover:   *subroutine for when it is game over
    lea     gameover_msg,A1
    move.b  #14,D0
    trap    #15
    move.b  #4,D0
    trap    #15
    cmp     #42,D1      * the answer
    beq     start
    bne     gameover
*------------------------------------------------------------
*----------------Fleeoing------------------------------------
*------------------------------------------------------------
flee:       * subroutine that determines if you can flee or not 
    move.l  enemy_speed_level,D2
    move.l  player_speed_level,D3
    cmp     D2,D3                   * if enemy speed is higher player can't flee
    bls     can_flee
    bsr     cant_flee
    
can_flee:   * subroutine executed if player can flee
    bsr     endl
    bsr     decorate
    lea     can_flee_msg,A1
    move.b  #14,D0
    trap    #15
    bsr     mission_select
    
cant_flee:  * subroutine executed if player can't flee   
    bsr     endl
    bsr     decorate
    lea     cant_flee_msg,A1
    move.b  #14,D0
    trap    #15
    bsr     attack
*-------------------------------------------------------
*---------------Screen Decoration-----------------------
*-------------------------------------------------------
decorate:
    move.b  #60, D3
    bsr     endl
out:
    lea     loop_msg,A1
    move.b  #14,D0
    trap    #15
	sub     #1,D3   decrement loop counter
    bne     out	    repeat until D0=0
    bsr     endl
    rts
    
clear_screen: 
    move.b  #11,D0      clear screen
    move.w  #$ff00,D1
    trap    #15
    rts

* subroutine that skips a line
endl:
    movem.l D0/A1,-(A7)
    move    #14,D0
    lea     crlf,A1
    trap    #15
    movem.l (A7)+,D0/A1
    rts
    
*-------------------------------------------------------
*-------------------Data Delarations--------------------
*-------------------------------------------------------
loop_msg:       dc.b    '.',0
crlf:           dc.b    $0D,$0A,0
welcome_msg:    dc.b    '************************************************************'
                dc.b    $0D,$0A
                dc.b    'MASS Effect Assembly Game'
                dc.b    $0D,$0A
                dc.b    'A game that might have better animation than Andromeda'
                dc.b    $0D,$0A
                dc.b    '************************************************************'
                dc.b    $0D,$0A,0
intro_msg       dc.b    'You are the newest member of the Spectres'
                dc.b    $0D,$0A
                dc.b    'A group of agents employed by the coucil'
                dc.b    $0D,$0A
                dc.b    'to handle threats to the galaxy.'
                dc.b    $0D,$0A
                dc.b    $0D,$0A
                dc.b    'You have been given your own ship'
                dc.b    $0D, $0A
                dc.b    'along with a skeleton crew.'
                dc.b    $0D,$0A
                dc.b    $0D,$0A
                dc.b    'You have been tasked with searching'
                dc.b    $0D,$0A
                dc.b    'the galaxy for threats.' ,0
citadel_msg     dc.b    'You are currently in the citadel'
                dc.b    $0D,$0A
                dc.b    'How do you wish to proceed'
                dc.b    $0D,$0A
                dc.b    '1) Go to ship dock (upgrade ship)'
                dc.b    $0D,$0A
                dc.b    '2) Leave the citadel (go on a mission)'
                dc.b    $0D,$0A
                dc.b    'Enter the corresponding number: ',0
credit_msg      dc.b    'The amount of credits you currently have is: ',0

upgrade_msg     dc.b    'You are currently at the ship docks'
                dc.b    $0D,$0A
                dc.b    'What do you wish to upgrade'
                dc.b    $0D,$0A
                dc.b    '1) Weapons'
                dc.b    $0D,$0A
                dc.b    '2) Shields'
                dc.b    $0D,$0A
                dc.b    '3) Ship Speed'
                dc.b    $0D,$0A
                dc.b    '4) Crew amount'
                dc.b    $0D,$0A
                dc.b    '5) Exit'
                dc.b    $0D,$0A
                dc.b    'Enter the corresponding number: ',0
  
weapon_level_msg    dc.b    'Your current weapon level is: ' ,0
shield_level_msg    dc.b    'Your current shield level is: ' ,0
speed_level_msg     dc.b    'Your current speed level is: ' ,0
crew_amount_msg     dc.b    'Your current crew amount is: ' ,0
weapon_upgrade_msg  dc.b    'To upgrade your weapons it will cost'
                    dc.b    $0D,$0A
                    dc.b    '500 credits'
                    dc.b    $0D,$0A
                    dc.b    'Do you wish to upgrade your weapons?'
                    dc.b    $0D,$0A
                    dc.b    '1) Yes     2) No' ,0
shield_upgrade_msg  dc.b    'To upgrade your shields it will cost'
                    dc.b    $0D,$0A
                    dc.b    '500 credits'
                    dc.b    $0D,$0A
                    dc.b    'Do you wish to upgrade your shields'
                    dc.b    $0D,$0A
                    dc.b    '1) Yes     2) No' ,0
speed_upgrade_msg   dc.b    'To upgrade your speed it will cost'
                    dc.b    $0D,$0A
                    dc.b    '500 credits'
                    dc.b    $0D,$0A
                    dc.b    'Do you wish to upgrade your speed'
                    dc.b    $0D,$0A
                    dc.b    '1) Yes     2) No' ,0
crew_upgrade_msg    dc.b    'To recruit 5 new crew members it will cost'
                    dc.b    $0D,$0A
                    dc.b    '1000 credits'
                    dc.b    $0D,$0A
                    dc.b    'Do you wish to recruit new crew members'
                    dc.b    $0D,$0A
                    dc.b    '1) Yes     2) No' ,0

error_msg           dc.b    'Invalid number entered try again',0

mission_select_msg  dc.b    'Dont Panic'
                    dc.b    $0D,$0A
                    dc.b    'You detect enemy activity nearby'
                    dc.b    $0D,$0A
                    dc.b    'It appears to be ',0
                    
batarian_enemy_msg  dc.b    'Batarian Slavers' ,0

cerberus_enemy_msg  dc.b    'Cerebus Forces',0

geth_enemy_msg      dc.b    'Geth',0
   
bluesuns_enemy_msg  dc.b    'Blue Suns Mercenaries',0

bloodpack_enemy_msg dc.b    'Blood Pack Mercenaries',0

eclipse_enemy_msg   dc.b    'Eclipse Mercenaries',0
   
reaper_enemy_msg    dc.b    'Reapers' ,0

location_choice_msg     dc.b    'Pick a destination'
                        dc.b    $0D,$0A
                        dc.b    '1) Return to Citadel'
                        dc.b    $0D,$0A
                        dc.b    '2) Omega'
                        dc.b    $0D,$0A
                        dc.b    '3) Skyllian Verge'
                        dc.b    $0D,$0A
                        dc.b    '4) Terminus System'
                        dc.b    $0D,$0A
                        dc.b    '5) Perseus Veil'
                        dc.b    $0D,$0A
                        dc.b    '6) Illium'
                        dc.b    $0D,$0A
                        dc.b    '7) Sol System'
                        dc.b    $0D,$0A
                        dc.b    '8) Wander About'
                        dc.b    $0D,$0A
                        dc.b    'Enter the corresponding number: ',0
  
battle_choice_msg       dc.b    'How do you wish to proceed'
                        dc.b    $0D,$0A
                        dc.b    '1) Attack Enemy'
                        dc.b    $0D,$0A
                        dc.b    '2) Flee' 
                        dc.b    $0D,$0A
                        dc.b    'Enter the corresponding number: ',0
can_flee_msg            dc.b    'You have succesfully fleed',0
cant_flee_msg           dc.b    'Enemy Speed too high unable to flee' ,0
enemy_health_msg        dc.b    'Enemy shields are: ',0
             
player_health_msg       dc.b   'Your shields are: ',0
win_msg                 dc.b    'You won the battle and have received more credits',0

lose_msg                dc.b    'You lost the battle'
                        dc.b    $0D,$0A
                        dc.b    'You have lost 5 crew members and'
                        dc.b    $0D,$0A
                        dc.b    'One level in ship weapons and speed',0
gameover_msg            dc.b    'You and your entire crew has died'
                        dc.b    $0D,$0A
                        dc.b    'Enter 42 to play again ',0


    end start






















*~Font name~Courier~
*~Font size~15~
*~Tab type~1~
*~Tab size~4~
